# Tech blogger

## Finding a topic

1. What technological challenge have you encountered recentrly?

- At work, creating a message broker on Raspberry Pi using Rust and NixOS
- At home, creating an ANSI color from image extractor using Rust

2. What new subject are you insterested in learning?

- At work, packaging C# libraries
- At home, packaging Rust binaries in a NixOS flake/package

3. What is your favourite field?

- At work, CSS website design
- At home, ricing my PC

4. What is your pain point?

- At work, finance calculations and Orchard Core undocumented behaviour
- At home, NixOS lack of support for Nvidia

5. What are the methodologies and development processes you use?

- Agile
- Kanban
- Standups

## Audience

My audience is [r/unixporn](https://reddit.com/r/unixporn).

## Main points

1. lulezojne
2. hyprland swww "hook"
3. waybar, dunst, ... "hooks"

## Introduction - What's in it for me

We're going to generate a complete system theme from your wallpaper.

## Finish - Call for action

Improve this system, rounding, corners, animations based on wallpaper?

## Problem solving

- Wal - couldn't fix transparency
- Lule - too old, lots of things I don't need
- Lulezojne - 500 LOC Rust binary no biggie

## Conclusion

Wild ride, but I have a system that I like and I hope it's good for you as well

## Types of blogs

### Code

- Carbon code on medium
- Github GIST for code on medium as well

### Overview

- excalidraw for visualizations

### Deep dive

I want to do this one for the Hyprland + NixOS + lulezojne post.

### Comparative analysis

Comparing for technologies.

## 7 C's

- Correct
- Comprehensive
- Concise
- Clear
- Compelling

## Editing

- Read out loud
- Accessibility (use HTML semantics)
- Terminology - make sure you use words that are widely known in your audience
- Explanatory verbiage - explain stuff you used
- Reference - base your blog on concrete sources
- Rhetorical question - the "Why?" thing i did for `lulezojne`
- Vary sentence length - keep things interesting
- Tone - don't be offensive be quirky
- Encouragement - encourage your audience
- Avoid 10$ words - KISS
- Avoid run-on sentences - long without any breaks should be avoided
- Avoid superflous words - stuff that adds no meaning
- Active voice/Passive voice - use active (subject => verb => object)
- Avoid negative and double negative - just confusing

### Tools

- capitalize my title - yes search that in google
- thesaurus - for synonyms, antonyms, ...
- grammarly - you want good grammar don't you?
- hemingway app - highlights things, passive voice, whats right what's wrong and how - actually rly cool
- quillbot - 
- pexel, pixabay, splash - free image

## Advice

- sometimes it's good enough
- ask for a peer review
- publish - profilicity
- write it all down - maybe create other blog posts from ideas of this one
- blogspace - like a workspace for blogging
- designate time
- make it a ritual - 5 min yoga, 30 min blogging stuff like that
- piece by piece - don't overwhelm yourself like you always do
- positive reinforcement - feel good after a blogging session
- accountability buddy - make someone accountable for your blog post (make someone monitor your blog progress)
- build an audience - commit to posting periodically so ppl expect a blog post
- unique voice - know thyself and your unique feels
- practice makes perfect - a bit by bit you improve your everyday communication skills IRL (yes yes good motivation gimme)
- keep on blogging
