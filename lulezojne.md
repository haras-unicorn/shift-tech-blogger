# How I came to creating lulezojne

I created a CLI tool that extracts the most prominent colors from an image of your choosing and maps them to ANSI.

The following is a story about how it came to be and how you can use it to theme your Linux desktop.

## Why?

I wanted to configure colors for all my linux GUI programs.
Sure, I could have made a theme for each one of them (waybar, wlogout, dunst, hyprland, gtk, qt, ...), but that was too much work for me in my head.

## Wal

Wal is a python program that extracts prominent colors from images and based on that changes the colors on all your configured programs.
Unfortunately for me, I couldn't get it to properly set transparency for my waybar and I didn't want to create a bash script for that.

## Lule

After some searching I stumbled onto lule. It does a similar thing to Wal but is written in Rust, generates more colors, has template support, and has a daemon mode that reloads your colors on demand.
Lule seemed great for my usecase - I wouldn't use all of it's functionality but I could make it load an image, generate colors and then generate proper colors with the templates.
Except, transparency is not a thing here as well, it didn't compile at all, it's a 3 year old project written in outdated Rust and after trying to refactor and modernized I just gave up.

## Lulezojne

So, after going through all that I figured - "Why not just make my own tool?". And that's how I made lulezojne in approximately one afternoon.
500 LOC's of Rust that does exactly what I need, concisely and is easily extendable and maintainable since I used only popular rust libraries (clap, serde, palette, handlebars, ...) or libraries that I can easily copy and paste into lulezojne (colorthief).

## Final setup

After writing and configuring lulezojne I wrote a bash script that uses swww to randomly pick my next wallpaper, generate color files for all my files using lulezojne and the picked wallpaper, and then executes a bunch of bash scripts that reload each of my configured files so that my lulezojne and swww configuration don't rely on those programs.

## In conclusion

If you wanna try out lulezojne check it out in my github repo.
You can use it like this
```
...
```
